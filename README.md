# Chroma Extension to allow and block websites

A simple extension that blocks all websites/domains except whitelisted ones to minimize distractions during Online School

installation Guideline

1. Clone this repository on your system
2. Open Chrome Extensions -> Customize & Control Google Chrome -> More Tools -> Extensions
3. Turn on Developer Mode using toggle button if not already done. (Top Right cornor)
4. Click on Load Unpacked -> This opens a filepicker, select the cloned folder.
5. Done
